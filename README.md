#  Jakarta EE 8 Challenge 

A Jakarta-EE application that generates pseudo-random numbers and exposes a REST API.

## Build

Make sure you have installed the latest JDK 8 and Apache Maven 3.6.

Execute the following command to build a clean package locally.

```bash
mvn clean package
```

## Execute
Currenlty the app is configured to run on TomEE

```bash
mvn package tomee:run
```

## API Operations available

GET http://localhost:8084/challenge/api/history                            
GET http://localhost:8084/challenge/api/pending                            
GET http://localhost:8084/challenge/api/cancelled                         
GET http://localhost:8084/challenge/api/stats                                
POST http://localhost:8084/challenge/api/random               
PUT http://localhost:8084/challenge/api/threads/{numOfThreads}       
PUT http://localhost:8084/challenge/api/{id}/cancel

## Resources 

* [The Cache Api](https://web.dev/cache-api-quick-guide/)
* [HTTP Caching](https://devcenter.heroku.com/articles/jax-rs-http-caching)
* [Simple CRUD REST API](https://kodnito.com/posts/simple-crud-rest-api-with-java-ee/)
