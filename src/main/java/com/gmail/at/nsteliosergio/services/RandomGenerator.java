package com.gmail.at.nsteliosergio.services;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RandomGenerator {    

    final Long minRand = 3L;
    final Long minWait = 30L;
    private ExecutorService executor;
    private static volatile RandomGenerator instance;

    private RandomGenerator(int nThreads) {        
        System.out.println("###### setting threads #######"  );
        executor = Executors.newFixedThreadPool(nThreads);
    }

    // private static class SingletonHelper{
    //     private static final RandomGenerator INSTANCE = new RandomGenerator();
    // }

    public static RandomGenerator getInstance(int nThreads){
        System.out.println("##### instantiating ######");
        // return SingletonHelper.INSTANCE;
        RandomGenerator result = instance;
        if(result != null){
            return result;
        }
        synchronized(RandomGenerator.class){
            if(instance == null){
                instance = new RandomGenerator(nThreads);
            }
            return instance;
        }

    }

    public Future<Long> generate(){        
        return executor.submit(() -> {
            Thread.sleep(minWait * 1000);
            return this.process();
        });
    }

    private Long process(){
        Long num = Long.valueOf(new Random().nextLong());
        return minRand.compareTo(num) > 0 ? process() : num;
    }
}