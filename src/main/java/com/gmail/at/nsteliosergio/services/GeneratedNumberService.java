package com.gmail.at.nsteliosergio.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import com.gmail.at.nsteliosergio.beans.GeneratedNumber;
import com.gmail.at.nsteliosergio.beans.ThreadsNumber;

@Stateless
public class GeneratedNumberService{

    public static final String PENDING = "PENDING";
    public static final String CANCELLED = "CANCELLED";
    public static final String FINISHED = "FINISHED";

    @PersistenceContext(unitName = "numbersPu")
    EntityManager entityManager;

    public void create(GeneratedNumber gNumber) {
        gNumber.setStatus(GeneratedNumberService.PENDING);
        entityManager.persist(gNumber);
    }

    public GeneratedNumber findById(String id){
        return entityManager.find(GeneratedNumber.class, id);
    }

    private List<GeneratedNumber> findByStatus(String status) {
        return entityManager.createNamedQuery("GeneratedNumber.findByStatus", GeneratedNumber.class)
                .setParameter("status", status).getResultList();
    }

    public List<GeneratedNumber> findPending(){
        return this.findByStatus(GeneratedNumberService.PENDING);
    }

    public List<GeneratedNumber> findFinished(){
        return this.findByStatus(GeneratedNumberService.FINISHED);
    }

    public List<GeneratedNumber> findCancelled(){
        return this.findByStatus(GeneratedNumberService.CANCELLED);
    }

    public List<GeneratedNumber> getAll() {
        return entityManager.createNamedQuery("GeneratedNumber.findAll", GeneratedNumber.class).getResultList();
    }

    public ThreadsNumber getThread(){

        if(entityManager.createNamedQuery("ThreadsNumber.findAll", ThreadsNumber.class).getResultList().size() == 0){
            this.createThread();
        }

        return entityManager.createNamedQuery("ThreadsNumber.findAll", ThreadsNumber.class).getSingleResult();
    }

    private void createThread() {
        entityManager.persist(new ThreadsNumber());
    }

    public void update(ThreadsNumber nThreads){
        entityManager.merge(nThreads);
    }

    public List<GeneratedNumber> getAllNumbers() {
        CriteriaQuery<GeneratedNumber> cq = entityManager.getCriteriaBuilder().createQuery(GeneratedNumber.class);
        cq.select(cq.from(GeneratedNumber.class));
        return entityManager.createQuery(cq).getResultList();
    }

    public void update(GeneratedNumber gNumber) {
        gNumber.setStatus(GeneratedNumberService.FINISHED);
        entityManager.merge(gNumber);
    }

    public void cancel(GeneratedNumber gNumber) {
        gNumber.setStatus(GeneratedNumberService.CANCELLED);
        entityManager.merge(gNumber);
    }

    public void delete(GeneratedNumber gNumber) {
        if (!entityManager.contains(gNumber)) {
            gNumber = entityManager.merge(gNumber);
        }

        entityManager.remove(gNumber);
    }

    public void clear() {
        Query removeAll = entityManager.createQuery("delete from GeneratedNumber");
        removeAll.executeUpdate();
    }
}