package com.gmail.at.nsteliosergio.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "threadsNumber")
@NamedQueries({ 
    @NamedQuery(name = "ThreadsNumber.findAll", query = "SELECT t FROM ThreadsNumber t")})
public class ThreadsNumber implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    
    @Column(columnDefinition = "long default 1")
    private Long numberOfThreads = 1L;

    public ThreadsNumber(){
        this.id = "1";
    }

    // public ThreadsNumber(Long numOfThreads){
    //     this.numberOfThreads = numOfThreads;
    // }

    /**
     * @return the numberOfThreads
     */
    public Long getNumberOfThreads() {
        return numberOfThreads;
    }

    /**
     * @param numberOfThreads the numberOfThreads to set
     */
    public void setNumberOfThreads(Long numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }
}
