package com.gmail.at.nsteliosergio.beans;

import java.time.ZonedDateTime;

import com.gmail.at.nsteliosergio.beans.GeneratedNumber;

public class GeneratedNumberBuilder{
    private String id;

    private Long randomNumber;

    private ZonedDateTime submittedTime;

    private ZonedDateTime finishedTime;

    public GeneratedNumberBuilder(){}

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the randomNumber
     */
    public Long getRandomNumber() {
        return randomNumber;
    }

    /**
     * @return the submittedTime
     */
    public ZonedDateTime getSubmittedTime() {
        return submittedTime;
    }
    
    /**
     * @return the finishedTime
     */
    public ZonedDateTime getFinishedTime() {
        return finishedTime;
    }

    /**
     * @param guid the guid to set
     */
    public GeneratedNumberBuilder setId(String id) {
        this.id = id;
        return this;
    }

    /**
     * @param randomNumber the randomNumber to set
     */
    public GeneratedNumberBuilder setRandomNumber(Long randomNumber) {
        this.randomNumber = randomNumber;
        return this;
    }

    /**
     * @param submittedTime the submittedTime to set
     */
    public GeneratedNumberBuilder setSubmittedTime(ZonedDateTime submittedTime) {
        this.submittedTime = submittedTime;
        return this;
    }

    public GeneratedNumber build(){
        return new GeneratedNumber(this);
    }

}