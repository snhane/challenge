package com.gmail.at.nsteliosergio.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "generatedNumbers")
@NamedQueries({ 
    @NamedQuery(name = "GeneratedNumber.findAll", query = "SELECT g FROM GeneratedNumber g"),
    @NamedQuery(name = "GeneratedNumber.findByStatus", query = "SELECT g FROM GeneratedNumber g WHERE g.status=:status") })
public class GeneratedNumber implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private Long randomNumber;

    private String submittedTime;

    private String finishedTime;

    private String updatedTime;

    private String status;

    private Long totalWaitingTimeSoFar;

    private Long processingTime;

    public GeneratedNumber() {
    }

    public GeneratedNumber(String id) {
        this.id = id;
    }

    public GeneratedNumber(GeneratedNumberBuilder builder) {
        this.setId(builder.getId());
        this.setSubmittedTime(builder.getSubmittedTime());
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the randomNumber
     */
    public Long getRandomNumber() {
        return randomNumber;
    }

    /**
     * @param randomNumber the randomNumber to set
     */
    public void setRandomNumber(Long randomNumber) {
        this.randomNumber = randomNumber;
    }

    /**
     * @param submittedTime the submittedTime to set
     */
    public void setSubmittedTime(ZonedDateTime submittedTime) {
        this.submittedTime = submittedTime.toLocalDateTime().toString();
    }

    public void setSubmittedTime(String submittedTime) {
        this.submittedTime = submittedTime;
    }

    /**
     * @param finishedTime the finishedTime to set
     */
    public void setFinishedTime(ZonedDateTime finishedTime) {
        this.finishedTime = finishedTime.toLocalDateTime().toString();

        this.processingTime = Long.valueOf(Duration
                .between(LocalDateTime.parse(this.getSubmittedTime()), finishedTime.toLocalDateTime()).getSeconds());
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the finishedTime
     */
    public String getFinishedTime() {
        return this.finishedTime;
    }

    /**
     * @return the submittedTime
     */
    public String getSubmittedTime() {
        return this.submittedTime;
    }

    /**
     * @param totalWaitingTimeSoFar the totalWaitingTimeSoFar to set
     */
    public void setTotalWaitingTimeSoFar(long totalWaitingTimeSoFar) {
        this.totalWaitingTimeSoFar = Long.valueOf(totalWaitingTimeSoFar);
    }

    @PreUpdate
    private void onUpdated() {
        this.updatedTime = ZonedDateTime.now().toLocalDateTime().toString();
    }

}