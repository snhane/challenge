package com.gmail.at.nsteliosergio.resources;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.gmail.at.nsteliosergio.beans.GeneratedNumber;
import com.gmail.at.nsteliosergio.services.GeneratedNumberService;
import com.gmail.at.nsteliosergio.services.RandomGenerator;
import com.gmail.at.nsteliosergio.beans.GeneratedNumberBuilder;
import com.gmail.at.nsteliosergio.beans.ThreadsNumber;

@Path("")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GeneratedNumberResource{

    final Long minRand = 3L;
    final Long minWait = 31L;
    final Long minThread = 1L;
    final Long maxThread = 10L;

    @Inject
    GeneratedNumberService gNumberService;

    @POST
    @Path("random")
    public Response random(@HeaderParam("X-Max-Wait") @DefaultValue("null") String xMaxWaitString){
        Long xMaxWait = Long.valueOf(Long.max(convertToLong(xMaxWaitString, minWait), minWait.longValue()));
        ZonedDateTime start = ZonedDateTime.now();
        String guid = UUID.randomUUID().toString();

        GeneratedNumber gen = new GeneratedNumberBuilder().setId(guid).setSubmittedTime(start).build();
        gNumberService.create(gen);

        // Future<Long> generatedNumber = new
        // RandomGenerator(gNumberService.getThread().getNumberOfThreads()).generate();
        Future<Long> generatedNumber = RandomGenerator
                .getInstance(gNumberService.getThread().getNumberOfThreads().intValue()).generate();

        try {
            Long result = xMaxWait.longValue() > minWait.longValue() ? generatedNumber.get(xMaxWait, TimeUnit.SECONDS)
                    : generatedNumber.get();

            gen.setRandomNumber(result);

            ZonedDateTime finish = ZonedDateTime.now();
            gen.setFinishedTime(finish);    
            gNumberService.update(gen);

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            System.out.println("Couldn't complete the task before the timeout.\nCancelling...");
            gNumberService.cancel(gen);            
        }        
        return this.composeResponse(Response.ok().build());
    }

    @GET
    @Path("history")
    public Response getAll() {
        return this.composeResponse(Response.ok(gNumberService.findFinished()).build());
    }

    @PUT
    @Path("{id}/cancel")
    public Response cancelRequest(@PathParam("id") String id){

        GeneratedNumber cNumber = gNumberService.findById(id);

        if(GeneratedNumberService.PENDING.equals(cNumber.getStatus())){
            gNumberService.cancel(cNumber);
        }

        return this.composeResponse(Response.ok().build());    
    }

    @GET
    @Path("stats")
    public Response stats() {
        return this.composeResponse(Response
                .ok(this.processStats(gNumberService.findFinished(), gNumberService.findPending().size())).build());
    }

    @GET
    @Path("pending")
    public Response pending() {
        return this.composeResponse(Response.ok(this.processPending(gNumberService.findPending())).build());
    }

    @GET
    @Path("cancelled")
    public Response cancelled() {
        return this.composeResponse(Response.ok(gNumberService.findCancelled()).build());
    }

    @PUT
    @Path("threads/{numOfThreads}")
    public Response theads(@PathParam("numOfThreads") String numOfThreadsString){
        Long nThreads = Long
                .valueOf(Long.min(convertToLong(numOfThreadsString, minThread).longValue(), maxThread.longValue()));

        ThreadsNumber numOfThreads = gNumberService.getThread();
        numOfThreads.setNumberOfThreads(nThreads);
        gNumberService.update(numOfThreads);

        return this.composeResponse(Response.ok(numOfThreads).build());    
    }

    /**
     * Converts a String to the Long value without throwing the exception
     * @param longString the string to convert
     * @param deafault the default value to assume (minimum)
     * @return the default value in case an exception is thrown
     */
    public Long convertToLong(String longString, Long deafault){
        return longString.chars().allMatch(Character::isDigit) ? Long.valueOf(longString) : deafault;
    }

    public String processStats(List<GeneratedNumber> generatedNumbers, int totalPending) {
        long maxDuration = Long.MIN_VALUE;
        long minDuration = Long.MAX_VALUE;

        for (GeneratedNumber generatedNumber : generatedNumbers) {

            Duration duration = Duration.between(LocalDateTime.parse(generatedNumber.getSubmittedTime()),
                    LocalDateTime.parse(generatedNumber.getFinishedTime()));
            
            long durationInSeconds = duration.getSeconds();

            minDuration = durationInSeconds < minDuration ? durationInSeconds : minDuration;

            maxDuration = durationInSeconds > maxDuration ? durationInSeconds : maxDuration;
        }

        return "{\"maximum wating time\" : " + maxDuration + ", \"minimum wating time\" : " + minDuration
                + ", \"total pending requests\" : " + totalPending + ", \"total cancelled requests\" : "
                + gNumberService.findCancelled().size() + "}";
    }

    public List<GeneratedNumber> processPending(List<GeneratedNumber> pending ) {
        ZonedDateTime now = ZonedDateTime.now();

        for (GeneratedNumber generatedNumber : pending) {
            generatedNumber.setTotalWaitingTimeSoFar(
                    Duration.between(LocalDateTime.parse(generatedNumber.getSubmittedTime()), now).getSeconds());
        }
        return pending;
    }

    /**
     * Adds the custom header to any resquest
     * @param response
     * @return
     */
    public Response composeResponse(Response response){
        response.getHeaders().add("X-Request-Duration", "xxx");
        System.out.println(response.getHeaders());
        //TODO use the date in the header to compute the duration
        return response;
    }

}