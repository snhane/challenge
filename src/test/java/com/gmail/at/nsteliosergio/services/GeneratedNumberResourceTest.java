package com.gmail.at.nsteliosergio.resources;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import com.gmail.at.nsteliosergio.resources.GeneratedNumberResource;

public class GeneratedNumberResourceTest {

    @Test
    public void shouldConvertStringToLong(){
        String text = "100";
        Long defaultVal = 2L;

        Long converted = new GeneratedNumberResource().convertToLong(text, defaultVal);
        assertTrue("Number was converted",converted.compareTo(100L)==0);
    }
}