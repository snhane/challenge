package com.gmail.at.nsteliosergio.beans;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import com.gmail.at.nsteliosergio.beans.GeneratedNumber;

public class GeneratedNumberTest{
    @Test
    public void shoulCreateGeneatedNumber(){
        GeneratedNumber gNumber = new GeneratedNumber("test");

        assertTrue("Object is not empty", gNumber!= null);
        assertTrue("the id is 'test'", "test".equals(gNumber.getId()));
    }
}