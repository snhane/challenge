package com.gmail.at.nsteliosergio.beans;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;

import com.gmail.at.nsteliosergio.beans.GeneratedNumber;
import com.gmail.at.nsteliosergio.beans.GeneratedNumberBuilder;

public class GeneratedNumberBuilderTest{

    @Test
    public void shouldBuildGeneratedNumber() {
        GeneratedNumber gen = new GeneratedNumberBuilder().setId("builder").setSubmittedTime(ZonedDateTime.now()).build();

        assertTrue("'builder' is the id of the generatedNumber", "builder".equals(gen.getId()));
    }

}