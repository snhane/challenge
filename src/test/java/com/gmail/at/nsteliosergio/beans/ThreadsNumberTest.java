package com.gmail.at.nsteliosergio.beans;

import com.gmail.at.nsteliosergio.beans.ThreadsNumber;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class ThreadsNumberTest{

    @Test
    public void testThreadsNumber() {
        ThreadsNumber threads = new ThreadsNumber();

        assertTrue("ThreadsNumber should be created with default number of threads",
                threads.getNumberOfThreads().equals(1L));
    }

    @Test
    public void testShouldUpdateNumberOfThreads(){
        ThreadsNumber threads = new ThreadsNumber();
        threads.setNumberOfThreads(3L);

        assertTrue("ThreadsNumber should be updated to3 threads",
                threads.getNumberOfThreads().equals(3L));

    }
}